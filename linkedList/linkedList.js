class Node {
    constructor(element) {
        this.element = element
        this.next = null
    }

}

class LinkedList {

    constructor() {
        this.head = null
        this.size = 0
    }

    // functions to be implemented 
    // add(element) 
    // insertAt(element, location) 
    // removeFrom(location) 
    // removeElement(element) 

    // Helper Methods 
    // isEmpty 
    // size_Of_List 
    // PrintList 

    add(element) {
        var node = new Node(element)
        var current
        if (this.head === null) {
            this.head = node //assign node to head if head null
        }
        else {
            current = this.head //assign current node pointed by head to current

            while (current.next) {
                current = current.next //iterate to last
            }
            current.next = node //add node at end

        }
        this.size++
    }
    insertAt(element, index) {
        var prev, current
        if (index < 0 || index > this.size) {
            return false //if index is negetive or greater than current size
        }
        else {
            var node = new Node(element)
            if (index === 0) {
                //index is 0 means need to insert element at start
                node.next = this.head //current head will be pointed by next of node
                this.head = node

            }
            else {
                //this case requires finding the prev and next node to the position
                //where node is inserted
                current = this.head
                let iterator = 0
                while (iterator < index) {
                    //iterate list until the iterator is less than index to insert
                    prev = current //maintain prev node and current node 
                    current = current.next
                    iterator++
                } // loop ends when inserting index reached
                //insert node
                node.next = current
                prev.next = node

            }
            this.size++
        }
    }

    removeFrom(index) {
        if (index < 0 || index > this.size) {
            return false
        }
        else {
            var curr, prev
            curr = this.head
            if (index === 0) {
                //remove first element and make next element as head 
                this.head = head.next

            }
            else {
                var it = 0 //iterator
                while (it < index) {
                    prev = curr
                    curr = curr.next
                    it++
                }
                //when found the index, delete item
                prev.next = curr.next
            }
            this.size--
            return curr.element
        }
    }

    removeElement(element) {
        //method removes the element when found
        var prev, curr

        if (this.head.element === element) { //if found at head
            this.head = head.next //assign head to next element
            return head.element
        }
        else {
            curr = this.head //assign head to current

            while (curr) {
                if (curr.element === element) { //item found
                    prev.next = curr.next
                    this.size--
                    return curr.element
                }
                prev = curr
                curr = curr.next
            }


        }

    }

    indexOf(element) {
        var index = 0
        var current = this.head
        while (current) {
            if (current.element === element) {
                return index
            }
            index++
            current = current.next
        }
        return -1
    }

    isEmpty() {
        return this.size === 0
    }

    sizeOfList() {
        return this.size
    }

    printList() {
        var current = this.head
        var str = ""
        while (current) {
            str += current.element + ","
            current = current.next
        }
        console.log(str)
    }

}

// creating an object for the 
// Linkedlist class 
var ll = new LinkedList();

// testing isEmpty on an empty list 
// returns true 
console.log(ll.isEmpty());

// adding element to the list 
ll.add(10);

// prints 10 
ll.printList();

// returns 1 
console.log(ll.sizeOfList());

// adding more elements to the list 
ll.add(20);
ll.add(30);
ll.add(40);
ll.add(50);

// returns 10 20 30 40 50 
ll.printList();

// prints 50 from the list 
console.log("is element removed ?" + ll.removeElement(50));

// prints 10 20 30 40 
ll.printList();

// returns 3 
console.log("Index of 40 " + ll.indexOf(40));

// insert 60 at second position 
// ll contains 10 20 60 30 40 
ll.insertAt(60, 2);

ll.printList();

// returns false 
console.log("is List Empty ? " + ll.isEmpty());

// remove 3rd element from the list 
console.log(ll.removeFrom(3));

// prints 10 20 60 40 
ll.printList();


