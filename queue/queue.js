class Queue {
  constructor() {
    this.queue = [];
  }

  //methods to implement
  //enqueue()
  //dequeue()
  //front()
  //isEmpty()
  //printQueue()

  enqueue(item) {
    this.queue.push(item);
  }
  dequeue() {
    if (this.queue.length) {
      return this.queue.shift();
    }
    return "underflow";
  }

  front() {
    if (this.queue.length) {
      return this.queue[0];
    }
    return "queue empty";
  }

  isEmpty() {
    return !!!this.queue.length;
  }

  printQueue() {
    if (this.queue.length) {
      for (let i = 0; i < this.queue.length; i++) {
        console.log(this.queue[i]);
      }
    } else {
      return "underflow";
    }
  }
}

let q = new Queue();

q.enqueue(3);
q.enqueue(2);
console.log(q.isEmpty());
q.dequeue();
q.dequeue();
console.log(q.isEmpty());
q.enqueue(5);
q.enqueue(3);
console.log(q.front());
q.printQueue();
