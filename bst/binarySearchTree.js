class Node {
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }
}

// binary tree with smaller nodes on left and heavier nodes on right
class BinarySearchTree {
  constructor() {
    this.root = null;
  }
  //methods
  //1. insertData(data)
  //2. removeData(data)

  // Helper function
  // findMinNode()
  // getRootNode()
  // inorder(node)
  // preorder(node)
  // postorder(node)
  // search(node, data)

  insertData(data) {
    let newNode = this.Node(data);
    if (!this.root) {
      this.root = newNode;
    } else {
      insertNode(this.root, newNode);
    }
  }

  insertNode(node, newNode) {
    if (node.data <= newNode.data) {
      //rootnode is less than new node
      if (!node.right) {
        node.right = newNode;
      } else {
        this.insertNode(node.right, newNode);
      }
    } else {
      if (node.data > newNode.data) {
        //append new node to left
        if (!node.left) {
          node.left = newNode;
        } else {
          this.insertNode(node.left, newNode);
        }
      }
    }
  }
}

removeData(data){
    this.root= this.removeNode(this.root,data)
}
removeNode(node,dataToDelete){
if(!node){
    return 'tree empty'
}
else {
    if(dataToDelete<node.data){//reinit the left node
        node.left= this.removeNode(node.left,dataToDelete)
        return node
    }
    else {
        node.right= this.removeNode(node.right,dataToDelete)
    }

    if(!node.right && !node.left) {
       node=null
       return node 
    }
    else if(node.left===null){
node= node.right
return node
    }
    else if(node.right===null){
        node= node.left
        return node
    }
}

}