class Stack {
    
    constructor(...items){
        this._dataStorage={} //to hold stack elements
        this.count=0
        
        if(items.length>0){     
           items.forEach(item=>{
               this._dataStorage[this.count]=item
              this.count++
           })
        }
    } 
   
    push(item){
        this._dataStorage[this.count]=item
        return this._dataStorage
    }

    pop(){
        if(!this.isEmpty())
        {
            

           let itemToPop=this._dataStorage[this.count] //array methods push and pop
           delete this._dataStorage[this.count]
           this.count--
           return itemToPop
        }
        else{
           return undefined
        }
  
    }

    peek(){
        
        return this._dataStorage[this.count]
    }

    size(){
        return   this.count
    }

    isEmpty(){
        return this.count===0
    }
}

var myStack = new Stack(3,4,5)
console.log(myStack.push(6))
console.log( myStack.peek())

console.log( myStack.pop())
console.log( myStack.peek())
console.log(myStack.push('gaurav'))
console.log( myStack.peek())

console.log( myStack.pop())
console.log( myStack.peek())
console.log( myStack.size())

